﻿using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Epam.Training.GroceryStore.Infrastructure.Facades
{
    public class AccountFacade : IAccountFacade
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["GroceryStore"].ConnectionString;

        public UserAccount Login(LoginModel model)
        {
            var user = new UserAccount();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_select_user_account", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = model.Email;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@password";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = model.Password;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        user.FirstName = dr.GetString(0);
                        user.LastName = dr.GetString(1);
                        user.Email = dr.GetString(2);
                        user.PhoneNumber = dr.GetString(3);
                        user.Address = dr.GetString(4);
                    }
                }

                connection.Close();

            }

            return user;
        }

        public bool Register(User model)
        {
            if (!IsUserInDb(model.email))
            {
                var wasInserted = false;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("Procedure_Insert_User", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@id";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = Guid.NewGuid();
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@email";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.email;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@phoneNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 25;
                    param.Value = model.phoneNumber;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@lastName";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.lastName;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@firstName";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.firstName;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@address";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.address;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@password";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.password;
                    sqlCmd.Parameters.Add(param);


                    // out parameter - check if new row (user) was inserted
                    param = new SqlParameter();
                    param.ParameterName = "@wasAdded";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    wasInserted = ((bool)sqlCmd.Parameters["@wasAdded"].Value);

                    connection.Close();
                }

                return wasInserted;
            }

            return false;
        }

        private bool IsUserInDb(string email)
        {
            bool isUserInDb = false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Check_If_User_In_DB", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = email;
                sqlCmd.Parameters.Add(param);

                // out parameter - check if user is already exists in DB
                param = new SqlParameter();
                param.ParameterName = "@isInDB";
                param.SqlDbType = SqlDbType.Bit;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                isUserInDb = ((bool)sqlCmd.Parameters["@isInDB"].Value);

                connection.Close();

            }

            return isUserInDb;
        }
    }
}
