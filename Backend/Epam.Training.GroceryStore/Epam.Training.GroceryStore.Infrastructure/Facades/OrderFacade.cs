﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.Facades
{
    public class OrderFacade : IOrderFacade
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["GroceryStore"].ConnectionString;

        public bool SubmitOrder(string id)
        {
            var wasUpdated = false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Update_Order_Status_Submit_Cart", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@id";
                param.SqlDbType = SqlDbType.UniqueIdentifier;
                param.Value = Guid.Parse(id);
                sqlCmd.Parameters.Add(param);

                // out parameter - check if row (order) was updated
                param = new SqlParameter();
                param.ParameterName = "@wasUpdated";
                param.SqlDbType = SqlDbType.Bit;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                wasUpdated = ((bool)sqlCmd.Parameters["@wasUpdated"].Value);

                connection.Close();
            }

            return wasUpdated;
        }

        private UserAccount GetUserOfOrder(string orderId)
        {
            var user = new UserAccount();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Select_User_Of_Order", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@id";
                param.SqlDbType = SqlDbType.UniqueIdentifier;
                param.Value = Guid.Parse(orderId);
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        user.FirstName = dr.GetString(0);
                        user.LastName = dr.GetString(1);
                        user.Email = dr.GetString(2);
                        user.PhoneNumber = dr.GetString(3);
                        user.Address = dr.GetString(4);
                    }
                }

                connection.Close();

            }

            return user;
        }

        public void SendEmail(string orderId)
        {
            var user = GetUserOfOrder(orderId);

            MailAddress from = new MailAddress("grocerystoregomel@gmail.com", "Grocery Store Administrator");
            MailAddress to = new MailAddress(user.Email);
            MailMessage m = new MailMessage(from, to);

            m.Subject = "Submit order";

            m.Body = "<h2>Your order has been received. <br> Representatives of our store will contact you as soon as it possible.</h2>";

            m.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);

            smtp.Credentials = new NetworkCredential("grocerystoregomel@gmail.com", "01010101");
            smtp.EnableSsl = true;
            smtp.Send(m);
        }

        public List<OrderInfo> UserOrders(string email)
        {
            var res = new List<OrderInfo>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Select_User_Orders_WFSC", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userEmail";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = email;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var item = new OrderInfo();
                        item.Id = dr.GetGuid(0);
                        item.UniqueCode = dr.GetString(1);
                        item.StatusId = Guid.Parse("2ba3afb1-7b91-4e18-898e-c6d551783750");
                        item.TotalPrice = decimal.Parse(dr.GetSqlMoney(2).ToString());

                        res.Add(item);
                    }
                }

                connection.Close();

            }

            return res;
        }
    }
}
