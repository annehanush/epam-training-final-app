﻿using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace Epam.Training.GroceryStore.Infrastructure.Facades
{
    public class ProductFacade : IProductFacade
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["GroceryStore"].ConnectionString;

        public List<Product> GetAllProductsOfCategory(Guid categoryId)
        {
            var resultList = new List<Product>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Select_All_Products_of_Category", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@categoryId";
                param.SqlDbType = SqlDbType.UniqueIdentifier;
                param.Value = categoryId;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var id = dr.GetGuid(0);
                        var name = dr.GetString(1);
                        var imagePath = dr.GetString(2);
                        var description = "";
                        try
                        {
                            description = dr.GetString(3);
                        }
                        catch (SqlNullValueException)
                        {
                            description = "";
                        }
                        decimal price = decimal.Parse(dr.GetSqlMoney(4).ToString());

                        var subcategory = new Subcategory { Id = dr.GetGuid(5), Name = dr.GetString(6) };

                        resultList.Add(new Product { Id = id, Name = name, ImagePath = imagePath, Description = description, Price = price, Subcategory = subcategory });
                    }
                }

                connection.Close();
            }

            return resultList;
        }
    }
}
