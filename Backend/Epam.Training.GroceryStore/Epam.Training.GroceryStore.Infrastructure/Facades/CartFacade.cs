﻿using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.SqlTypes;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.Facades
{
    public class CartFacade : ICartFacade
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["GroceryStore"].ConnectionString;

        public Order GetUserCart(string email)
        {
            var order = new Order();
            var orderItems = new List<OrderItem>();

            var existingOrderId = GetIdOfOrderInWFBCStatus(email);

            if (existingOrderId != null)
            {
                order.Id = existingOrderId.Value;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("Procedure_Select_Items_Of_Order", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@orderId";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = order.Id;
                    sqlCmd.Parameters.Add(param);

                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var item = new OrderItem
                            {
                                Id = dr.GetGuid(0),
                                ProductName = dr.GetString(1),
                                ProductImage = dr.GetString(2),
                                ProductPrice = decimal.Parse(dr.GetSqlMoney(3).ToString()),
                                ProductDescription = dr.GetString(4)
                            };

                            orderItems.Add(item);
                        }
                    }

                    connection.Close();
                }
            }

            order.OrderItems = orderItems;

            return order;
        }

        public bool AddToCart(AddToCartModel model)
        {
            var existingOrderId = GetIdOfOrderInWFBCStatus(model.UserEmail);

            // if order exists
            if (existingOrderId != null)
            {
                var wasAdded = false;

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("Procedure_Insert_OrderItem", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@productId";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = Guid.Parse(model.ProductId);
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@orderId";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = existingOrderId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@id";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = Guid.NewGuid();
                    sqlCmd.Parameters.Add(param);

                    // out parameter - check if new row (order item) was inserted
                    param = new SqlParameter();
                    param.ParameterName = "@wasAdded";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    wasAdded = ((bool)sqlCmd.Parameters["@wasAdded"].Value);

                    connection.Close();
                }

                return wasAdded;
            }
            else
            {
                // new order
                var orderId = Guid.NewGuid();
                var orderUniqueNumber = DateTime.Now.ToString("yyyy-MM-dd") + orderId;

                // adding new order
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("Procedure_Insert_Order", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@id";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = orderId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@uniqueCode";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = orderUniqueNumber;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@userEmail";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = model.UserEmail;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();
                }

                var wasAdded = false;

                // adding order item to new order
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("Procedure_Insert_OrderItem", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@productId";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = Guid.Parse(model.ProductId);
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@orderId";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = orderId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@id";
                    param.SqlDbType = SqlDbType.UniqueIdentifier;
                    param.Value = Guid.NewGuid();
                    sqlCmd.Parameters.Add(param);

                    // out parameter - check if new row (order item) was inserted
                    param = new SqlParameter();
                    param.ParameterName = "@wasAdded";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    wasAdded = ((bool)sqlCmd.Parameters["@wasAdded"].Value);

                    connection.Close();
                }

                return wasAdded;
            }
        }

        private Guid? GetIdOfOrderInWFBCStatus(string userEmail)
        {
            Guid? res = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Select_Order_Id_In_Status_WFBS_By_User_Email", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = userEmail;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        res = dr.GetGuid(0);
                    }
                }

                connection.Close();
            }

            return res;
        }

        public void RemoveCartItem(string id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Delete_Order_Item", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@id";
                param.SqlDbType = SqlDbType.UniqueIdentifier;
                param.Value = Guid.Parse(id);
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
