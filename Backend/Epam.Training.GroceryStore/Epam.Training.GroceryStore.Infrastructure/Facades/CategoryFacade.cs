﻿using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Epam.Training.GroceryStore.Infrastructure.Facades
{
    public class CategoryFacade : ICategoryFacade
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["GroceryStore"].ConnectionString;

        public List<Category> GetAllCategories()
        {
            var resultList = new List<Category>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_Select_All_Categories", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Guid id = dr.GetGuid(0);
                        string name = dr.GetString(1);
                        string imagePath = dr.GetString(2);

                        resultList.Add(new Category { Id = id, Name = name, ImagePath = imagePath });
                    }
                }

                connection.Close();
            }

            return resultList;
        }
    }
}
