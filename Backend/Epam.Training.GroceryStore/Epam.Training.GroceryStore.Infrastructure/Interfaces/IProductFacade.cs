﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using System;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.Interfaces
{
    public interface IProductFacade
    {
        List<Product> GetAllProductsOfCategory(Guid categoryId);
    }
}
