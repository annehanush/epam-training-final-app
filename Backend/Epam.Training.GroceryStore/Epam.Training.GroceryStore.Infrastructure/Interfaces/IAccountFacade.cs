﻿using Epam.Training.GroceryStore.Infrastructure.DTO;

namespace Epam.Training.GroceryStore.Infrastructure.Interfaces
{
    public interface IAccountFacade
    {
        bool Register(User model);
        UserAccount Login(LoginModel model);
    }
}
