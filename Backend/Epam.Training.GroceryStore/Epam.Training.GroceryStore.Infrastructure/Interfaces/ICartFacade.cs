﻿using Epam.Training.GroceryStore.Infrastructure.DTO;

namespace Epam.Training.GroceryStore.Infrastructure.Interfaces
{
    public interface ICartFacade
    {
        bool AddToCart(AddToCartModel model);
        Order GetUserCart(string email);
        void RemoveCartItem(string id);
    }
}
