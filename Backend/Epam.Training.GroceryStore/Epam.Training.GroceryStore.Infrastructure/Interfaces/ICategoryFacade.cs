﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.Interfaces
{
    public interface ICategoryFacade
    {
        List<Category> GetAllCategories();
    }
}
