﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.Interfaces
{
    public interface IOrderFacade
    {
        bool SubmitOrder(string id);
        void SendEmail(string orderId);
        List<OrderInfo> UserOrders(string email);
    }
}
