﻿using System;

namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class OrderInfo
    {
        public Guid Id { get; set; }
        public string UniqueCode { get; set; }
        public Guid StatusId { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
