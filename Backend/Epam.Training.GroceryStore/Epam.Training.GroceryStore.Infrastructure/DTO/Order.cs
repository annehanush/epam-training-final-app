﻿using System;
using System.Collections.Generic;

namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class Order
    {
        public Guid Id { get; set; }
        public List<OrderItem> OrderItems { get; set; }
    }
}
