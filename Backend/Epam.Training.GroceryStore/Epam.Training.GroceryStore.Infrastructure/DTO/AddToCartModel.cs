﻿namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class AddToCartModel
    {
        public string ProductId { get; set; }
        public string UserEmail { get; set; }
    }
}
