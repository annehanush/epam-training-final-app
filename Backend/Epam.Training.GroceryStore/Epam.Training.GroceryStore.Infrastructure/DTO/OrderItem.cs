﻿using System;

namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class OrderItem
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductDescription { get; set; }
    }
}
