﻿using System;

namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Subcategory Subcategory { get; set; }
    }
}
