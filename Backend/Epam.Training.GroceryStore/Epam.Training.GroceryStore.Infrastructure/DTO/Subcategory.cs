﻿using System;

namespace Epam.Training.GroceryStore.Infrastructure.DTO
{
    public class Subcategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
