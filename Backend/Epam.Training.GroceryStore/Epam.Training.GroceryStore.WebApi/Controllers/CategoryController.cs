﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class CategoryController : ApiController
    {
        ICategoryFacade _categoryFacade;

        public CategoryController(ICategoryFacade categoryFacade)
        {
            _categoryFacade = categoryFacade;
        }

        [HttpGet]
        public List<Category> Get()
        {
            return _categoryFacade.GetAllCategories();
        }
    }
}