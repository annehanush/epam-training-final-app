﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System.Net;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class RegistrationController : ApiController
    {
        IAccountFacade _accountFacade;

        public RegistrationController(IAccountFacade accountFacade)
        {
            _accountFacade = accountFacade;
        }

        public IHttpActionResult Post([FromBody]User model)
        {
            if (_accountFacade.Register(model))
            {
                return Created("User", model);
            }
            else
            {
                // if user already exists
                return StatusCode(HttpStatusCode.NoContent);
            }
        }
    }
}