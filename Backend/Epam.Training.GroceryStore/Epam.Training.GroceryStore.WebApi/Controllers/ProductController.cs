﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class ProductController : ApiController
    {
        IProductFacade _productFacade;

        public ProductController(IProductFacade productFacade)
        {
            _productFacade = productFacade;
        }

        [HttpGet]
        /// <summary>
        /// List of products of selected category.
        /// </summary>
        /// <returns></returns>
        public List<Product> Get(Guid categoryId)
        {
            if (categoryId != null)
            {
                return _productFacade.GetAllProductsOfCategory(categoryId);
            }
            else
            {
                return new List<Product>();
            }
        }
    }
}