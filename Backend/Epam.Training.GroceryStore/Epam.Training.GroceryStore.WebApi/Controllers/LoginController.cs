﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System.Net;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class LoginController : ApiController
    {
        IAccountFacade _accountFacade;

        public LoginController(IAccountFacade accountFacade)
        {
            _accountFacade = accountFacade;
        }

        public IHttpActionResult Post([FromBody]LoginModel model)
        {
            var user = _accountFacade.Login(model);

            if (user.Email == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return Created("User", user);
            }
            
        }
    }
}