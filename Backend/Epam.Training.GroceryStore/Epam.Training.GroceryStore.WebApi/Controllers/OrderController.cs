﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class OrderController : ApiController
    {
        IOrderFacade _orderFacade;

        public OrderController(IOrderFacade orderFacade)
        {
            _orderFacade = orderFacade;
        }

        public List<OrderInfo> Get(string userEmail)
        {
            return _orderFacade.UserOrders(userEmail);
        }

        public IHttpActionResult Post([FromBody]string id)
        {
            if (_orderFacade.SubmitOrder(id))
            {
                _orderFacade.SendEmail(id);
                return Ok();
            }
            else
            {
                return InternalServerError();
            }
        }
    }
}