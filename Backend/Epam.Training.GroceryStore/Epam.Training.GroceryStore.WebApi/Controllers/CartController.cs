﻿using Epam.Training.GroceryStore.Infrastructure.DTO;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using System.Web.Http;

namespace Epam.Training.GroceryStore.WebApi.Controllers
{
    public class CartController : ApiController
    {
        ICartFacade _cartFacade;

        public CartController(ICartFacade cartFacade)
        {
            _cartFacade = cartFacade;
        }

        public Order Get(string userEmail)
        {
            return _cartFacade.GetUserCart(userEmail);
        }

        [Route("api/Cart/Add")]
        public IHttpActionResult Add([FromBody]AddToCartModel model)
        {
            if (_cartFacade.AddToCart(model))
            {
                return Ok();
            }
            else
            {
                return InternalServerError();
            }
        }

        [Route("api/Cart/Delete")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]string id)
        {
            _cartFacade.RemoveCartItem(id);
            return Ok();
        }
    }
}