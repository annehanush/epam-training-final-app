using System.Web.Http;
using WebActivatorEx;
using Epam.Training.GroceryStore.WebApi;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Epam.Training.GroceryStore.WebApi
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration 
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "Epam.Training.GroceryStore.WebApi");
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}