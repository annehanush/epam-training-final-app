﻿using Epam.Training.GroceryStore.Infrastructure.Facades;
using Epam.Training.GroceryStore.Infrastructure.Interfaces;
using Epam.Training.GroceryStore.WebApi.Infrastructure;
using Microsoft.Practices.Unity;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Epam.Training.GroceryStore.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API routes 
            config.MapHttpAttributeRoutes();

            // Web API configuration and services
            var container = new UnityContainer();

            container.RegisterType<ICategoryFacade, CategoryFacade>(new HierarchicalLifetimeManager());
            container.RegisterType<IProductFacade, ProductFacade>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountFacade, AccountFacade>(new HierarchicalLifetimeManager());
            container.RegisterType<ICartFacade, CartFacade>(new HierarchicalLifetimeManager());
            container.RegisterType<IOrderFacade, OrderFacade>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, action = RouteParameter.Optional }
            );
        }
    }
}
