﻿CREATE TABLE [dbo].[Subcategory]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [CategoryId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_Subcategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category]([Id])
)
