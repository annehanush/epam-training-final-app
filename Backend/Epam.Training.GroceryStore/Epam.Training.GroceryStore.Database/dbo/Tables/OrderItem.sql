﻿CREATE TABLE [dbo].[OrderItem]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ProductId] UNIQUEIDENTIFIER NOT NULL, 
    [OrderId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_OrderItem_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product]([Id]), 
    CONSTRAINT [FK_OrderItem_Order] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Order]([Id])
)
