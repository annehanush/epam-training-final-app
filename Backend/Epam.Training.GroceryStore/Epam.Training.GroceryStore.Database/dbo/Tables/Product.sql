﻿CREATE TABLE [dbo].[Product]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [ImagePath] NVARCHAR(MAX) NOT NULL, 
    [Price] MONEY NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [SubcategoryId] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_Product_Subcategory] FOREIGN KEY ([SubcategoryId]) REFERENCES [dbo].[Subcategory]([Id])
)
