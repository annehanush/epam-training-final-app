﻿CREATE PROCEDURE [dbo].[Procedure_Insert_User]
	@id UNIQUEIDENTIFIER,
	@email NVARCHAR(50),
	@phoneNumber NVARCHAR(25),
	@lastName NVARCHAR(50),
	@firstName NVARCHAR(50),
	@address NVARCHAR(50),
	@password NVARCHAR(50),
	@wasAdded BIT OUTPUT
AS
BEGIN

	INSERT INTO [dbo].[User] VALUES (@id, @email, @phoneNumber, @lastName, @firstName, @address, @password)
	SET @wasAdded = @@ROWCOUNT

END