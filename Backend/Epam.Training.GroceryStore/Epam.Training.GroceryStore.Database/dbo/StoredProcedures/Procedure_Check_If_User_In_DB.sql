﻿CREATE PROCEDURE [dbo].[Procedure_Check_If_User_In_DB]
	@email NVARCHAR(50),
	@isInDB BIT OUTPUT
AS
	IF ( ( SELECT COUNT(*) FROM [dbo].[User] WHERE Email = @email ) > 0)
	BEGIN
		SET @isInDB = 1
	END
	ELSE
		SET @isInDB = 0
