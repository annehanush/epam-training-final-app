﻿CREATE PROCEDURE [dbo].[Procedure_Select_User_Orders_WFSC]
	@userEmail NVARCHAR(50)
AS
	SELECT [dbo].[Order].[Id], UniqueOrderCode, SUM(Price) as 'TotalPrice'
	FROM [dbo].[Order], [dbo].[OrderItem], [dbo].[Product]
	WHERE OrderId = [dbo].[Order].[Id] AND ProductId = [dbo].[Product].[Id] AND StatusId = '2ba3afb1-7b91-4e18-898e-c6d551783750' AND UserId = ( SELECT Id FROM [dbo].[User] WHERE Email = @userEmail )
	GROUP BY [dbo].[Order].[Id], UniqueOrderCode