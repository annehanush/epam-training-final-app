﻿CREATE PROCEDURE [dbo].[Procedure_Delete_Order_Item]
	@id UNIQUEIDENTIFIER
AS
	DELETE FROM [dbo].[OrderItem] WHERE Id = @id
