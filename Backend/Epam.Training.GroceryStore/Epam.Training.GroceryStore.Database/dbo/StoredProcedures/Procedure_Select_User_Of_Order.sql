﻿CREATE PROCEDURE [dbo].[Procedure_Select_User_Of_Order]
	@id UNIQUEIDENTIFIER
AS
	SELECT FirstName, LastName, Email, PhoneNumber, Address FROM [dbo].[User] WHERE Id = ( SELECT UserId FROM [dbo].[Order] WHERE Id = @id )
