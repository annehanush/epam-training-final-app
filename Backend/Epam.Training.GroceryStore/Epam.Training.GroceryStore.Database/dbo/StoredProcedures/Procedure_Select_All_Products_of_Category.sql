﻿CREATE PROCEDURE [dbo].[Procedure_Select_All_Products_of_Category]
	@categoryId uniqueidentifier
AS
	SELECT [Product].[Id], [Product].[Name], ImagePath, [Product].[Description], Price, [Subcategory].[Id], [Subcategory].[Name]
	FROM Product, Subcategory
	WHERE SubcategoryId = [Subcategory].[Id] AND CategoryId = @categoryId
