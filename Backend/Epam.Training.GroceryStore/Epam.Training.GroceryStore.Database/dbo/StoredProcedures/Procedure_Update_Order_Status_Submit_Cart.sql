﻿CREATE PROCEDURE [dbo].[Procedure_Update_Order_Status_Submit_Cart]
	@id UNIQUEIDENTIFIER,
	@wasUpdated BIT OUTPUT
AS
	UPDATE [dbo].[Order] SET StatusId = '2ba3afb1-7b91-4e18-898e-c6d551783750'
	WHERE Id = @id
	SET @wasUpdated = @@ROWCOUNT
