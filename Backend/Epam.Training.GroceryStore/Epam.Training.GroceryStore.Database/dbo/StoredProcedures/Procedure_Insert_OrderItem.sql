﻿CREATE PROCEDURE [dbo].[Procedure_Insert_OrderItem]
	@productId UNIQUEIDENTIFIER,
	@orderId UNIQUEIDENTIFIER,
	@id UNIQUEIDENTIFIER,
	@wasAdded BIT OUTPUT
AS
	BEGIN

	INSERT INTO [dbo].[OrderItem] VALUES (@id, @productId, @orderId)
	SET @wasAdded = @@ROWCOUNT

END
