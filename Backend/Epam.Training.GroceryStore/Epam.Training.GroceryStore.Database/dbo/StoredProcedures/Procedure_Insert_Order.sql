﻿CREATE PROCEDURE [dbo].[Procedure_Insert_Order]
	@id UNIQUEIDENTIFIER,
	@uniqueCode NVARCHAR(50),
	@userEmail NVARCHAR(50)
AS
BEGIN
	DECLARE @userId UNIQUEIDENTIFIER

	SELECT @userId = Id FROM [dbo].[User] WHERE Email = @userEmail

	INSERT INTO [dbo].[Order] VALUES (@id, @uniqueCode, '6697f472-6067-4f8a-86c4-98801ba7c4d8', @userId)

END
