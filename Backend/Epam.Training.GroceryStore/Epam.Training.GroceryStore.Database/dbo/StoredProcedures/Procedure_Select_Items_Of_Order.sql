﻿CREATE PROCEDURE [dbo].[Procedure_Select_Items_Of_Order]
	@orderId UNIQUEIDENTIFIER
AS
	SELECT [OrderItem].Id, Name, ImagePath, Price, Description 
	FROM [dbo].[OrderItem], [dbo].[Product]
	WHERE OrderId = @orderId AND ProductId = [Product].Id
