INSERT INTO [dbo].[Category] VALUES ('2204cf3f-c5b7-4221-88a1-65e4fb870ec1', 'Fruits and vegetables', 'https://www.bigbasket.com/media/customPage/aae47f13-f754-4134-a290-2c672d003ca3/d2cf26ac-7042-4679-8f1e-1c6820688307/2a5ab88b-7abd-46db-a7a9-c882f4052e62/Organic-fruits-&-vegetables.jpg')
INSERT INTO [dbo].[Category] VALUES ('854e7fcf-61e2-4769-a2ea-fe1293e06d18', 'Bakery products', 'http://www.interactive-biology.com/wp-content/uploads/2012/06/dairy-products-and-eggs-1024x762.jpg')
INSERT INTO [dbo].[Category] VALUES ('44462326-a9fa-4374-baf0-69fc335fd72d', 'Dairy products and eggs', 'http://21425-presscdn.pagely.netdna-cdn.com/wp-content/uploads/2015/10/Bakery-Products.jpg')
INSERT INTO [dbo].[Category] VALUES ('5f5ab921-6ec7-4899-8ec2-c14cd6d38555', 'Meat, fish, poultry', 'https://everybodysfitoceanside.com/wp-content/uploads/2017/03/hEALTHY-PROTEINS-2.jpg')
INSERT INTO [dbo].[Category] VALUES ('58cabe4c-d1a4-44d3-8b21-f708531e0c84', 'Frozen food', 'http://www.quaysidewholesale.co.uk/wp-content/uploads/2011/07/products-home.png')
GO

INSERT INTO [dbo].[Subcategory] VALUES ('f29c38f7-b5c5-4c00-8576-578196a92277', 'Fruits', '2204cf3f-c5b7-4221-88a1-65e4fb870ec1')
INSERT INTO [dbo].[Subcategory] VALUES ('11b7cf32-1646-433b-95a4-fca1e7a75b2a', 'Vegetables', '2204cf3f-c5b7-4221-88a1-65e4fb870ec1')
INSERT INTO [dbo].[Subcategory] VALUES ('cbc9da60-e29f-41fc-9514-fcfa41ab32ab', 'Milk', '44462326-a9fa-4374-baf0-69fc335fd72d')
GO

INSERT INTO [dbo].[Product] VALUES ('82065194-09ba-439a-a555-cba1be263765', 'Banana', 'http://cdn-hn.arrowpress.net/efarm/wp-content/uploads/2017/07/33-283x283.jpg', 1.59, '1kg', 'f29c38f7-b5c5-4c00-8576-578196a92277')
INSERT INTO [dbo].[Product] VALUES ('e0faf20d-e16d-4b9a-b819-80b53930c6b4', 'Pineapple', 'http://cdn-hn.arrowpress.net/efarm/wp-content/uploads/2017/05/5-283x283.jpg', 2.49, '1kg', 'f29c38f7-b5c5-4c00-8576-578196a92277')
INSERT INTO [dbo].[Product] VALUES ('82df6972-e8dd-4259-befd-5b71ee191d6b', 'Radish', 'http://cdn-hn.arrowpress.net/efarm/wp-content/uploads/2017/07/17-283x283.jpg', 2.99, '1kg', '11b7cf32-1646-433b-95a4-fca1e7a75b2a')
INSERT INTO [dbo].[Product] VALUES ('544f609f-4c5c-4f5e-a1fd-b4bf8fea9be1', 'Cabbage', 'http://cdn-hn.arrowpress.net/efarm/wp-content/uploads/2017/02/10-283x283.jpg', 0.49, '1kg', '11b7cf32-1646-433b-95a4-fca1e7a75b2a')
GO

INSERT INTO [dbo].[Status] VALUES ('6697f472-6067-4f8a-86c4-98801ba7c4d8', 'Waiting for buyer confirmation')
INSERT INTO [dbo].[Status] VALUES ('2ba3afb1-7b91-4e18-898e-c6d551783750', 'Waiting for seller confirmation')
INSERT INTO [dbo].[Status] VALUES ('77cb8ca6-6970-4356-b19e-737fc537a5de', 'Rejected')
INSERT INTO [dbo].[Status] VALUES ('3da58abc-ac3b-4e4d-958d-8312760f31fe', 'Confirmed')
GO