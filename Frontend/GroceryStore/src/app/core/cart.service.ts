import { Injectable } from '@angular/core';

import { UrlService } from './url.service';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { IAddToCart } from './models/IAddToCart';
import { AccountService } from './account.service';
import { IOrder } from './models/IOrder';

import { ToastrService } from 'ngx-toastr';

@Injectable()
export class CartService {

  constructor(private urlService: UrlService,
              private restService: RestService,
              public http: Http,
              private accountService: AccountService,
              private router: Router,
              private toastr: ToastrService) { }

  getCartItems(): Observable<IOrder> {
    if (this.accountService.isAuthenticated) {
      const url = this.urlService.cartUrl + this.accountService.authenticatedUser.Email;
      return this.restService.get(url);
    } else {
      this.router.navigate(['/login']);
    }
  }

  addCartItem(model: IAddToCart) {
    const url = this.urlService.addToCart;

    const myHeaders = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    const options = new RequestOptions({ headers: myHeaders });
    const body = JSON.stringify(model);

    this.http.post(url, body, options).subscribe(reg => {
      if (reg.status === 200) {
        this.toastr.success('New product was added to your cart', 'CART', );
      } else if (reg.status === 500) {
        this.toastr.error('Smth goes wrong.... Internal server error.', 'ERROR', );
      }
    });
  }

  removeCartItem(id: string) {
    const url = this.urlService.removeFromCart;

    const myHeaders = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    const options = new RequestOptions({ headers: myHeaders });
    const body = JSON.stringify(id);

    this.http.post(url, body, options).subscribe(reg => {
    });
  }

  submitCart(id: string) {
    const url = this.urlService.submitCartOrderUrl;

    const myHeaders = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    const options = new RequestOptions({ headers: myHeaders });
    const body = JSON.stringify(id);

    this.http.post(url, body, options).subscribe(reg => {
      if (reg.status === 200) {
        alert('Your order has been received. Wait for our employee to connect with you.');
        this.router.navigate(['/home']);
      } else if (reg.status === 500) {
        alert('Smth goes wrong.... Internal server error.');
      }
    });
  }

}
