import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';
import { UrlService } from './url.service';
import { CategoryService } from './category.service';
import { IProduct } from './models/IProduct';

@Injectable()
export class ProductsCatalogService {

  constructor(private urlService: UrlService,
              private restService: RestService,
              private categoryService: CategoryService,
              public http: Http,
              private router: Router) { this.http = http; }

  getProductsCatalog(): Observable<IProduct[]> {
    const selectedCategory = this.categoryService.selectedCategory;

    const url = this.urlService.productsCatalogUrl + selectedCategory.Id;
    return this.restService.get(url);
  }

}
