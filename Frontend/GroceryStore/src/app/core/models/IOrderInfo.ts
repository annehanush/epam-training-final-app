export interface IOrderInfo {
    Id: string;
    UniqueCode: string;
    StatusId: string;
    TotalPrice: number;
}
