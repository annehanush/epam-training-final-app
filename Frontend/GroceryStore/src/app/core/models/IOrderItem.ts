export interface IOrderItem {
    Id: string;
    ProductName: string;
    ProductImage: string;
    ProductPrice: number;
    ProductDescription: string;
}
