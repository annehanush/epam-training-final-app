export interface ICategory {
    Id: string;
    Name: string;
    ImagePath: string;
}
