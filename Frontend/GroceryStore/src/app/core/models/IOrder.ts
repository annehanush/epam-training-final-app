import { IOrderItem } from './IOrderItem';

export interface IOrder {
    Id: string;
    OrderItems: IOrderItem[];
}
