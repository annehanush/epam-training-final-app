export interface IUser {
    FirstName: string;
    LastName: string;
    Email: string;
    PhoneNumber: string;
    Address: string;
}
