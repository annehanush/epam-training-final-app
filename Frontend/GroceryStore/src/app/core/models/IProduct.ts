import { ISubcategory } from './ISubcategory';

export interface IProduct {
    Id: string;
    Name: string;
    ImagePath: string;
    Description: string;
    Price: number;
    Subcategory: ISubcategory;
}
