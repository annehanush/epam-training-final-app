import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class UrlService {

  baseUrl: string;

  categoryUrl: string;
  productsCatalogUrl: string;
  register: string;
  login: string;
  cartUrl: string;
  addToCart: string;
  removeFromCart: string;
  submitCartOrderUrl: string;
  userOrdersUrl: string;

  constructor() {
    this.baseUrl = environment.apiPath;

    this.initUrls();
  }

    initUrls() {
      this.categoryUrl = `${this.baseUrl}/Category`;

      this.productsCatalogUrl = `${this.baseUrl}/Product?categoryId=`;

      this.register = `${this.baseUrl}/Registration`;
      this.login = `${this.baseUrl}/Login`;

      this.cartUrl = `${this.baseUrl}/Cart?userEmail=`;
      this.addToCart = `${this.baseUrl}/Cart/Add`;
      this.removeFromCart = `${this.baseUrl}/Cart/Delete`;

      this.submitCartOrderUrl = `${this.baseUrl}/Order`;
      this.userOrdersUrl = `${this.baseUrl}/Order?userEmail=`;
    }

}
