import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { CategoryService } from './category.service';
import { UrlService } from './url.service';
import { RestService } from './rest.service';
import { ProductsCatalogService } from './products-catalog.service';
import { AccountService } from './account.service';
import { CartService } from './cart.service';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [],
  providers: [
    RestService,
    CategoryService,
    UrlService,
    ProductsCatalogService,
    AccountService,
    CartService
  ]
})
export class CoreModule { }
