import { Injectable } from '@angular/core';

import { IRegister } from './models/IRegister';
import { IUser } from './models/IUser';
import { UrlService } from './url.service';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ILogin } from './models/ILogin';
import { IOrderInfo } from './models/IOrderInfo';
import { RestService } from './rest.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AccountService {

  isAuthenticated = false;
  authenticatedUser: IUser = null;

  constructor(private urlService: UrlService,
              public http: Http,
              private router: Router,
              private restService: RestService,
              private toastr: ToastrService) { }

  login(model: ILogin) {
    const url = this.urlService.login;

    const myHeaders = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    const options = new RequestOptions({ headers: myHeaders });
    const body = JSON.stringify(model);

    this.http.post(url, body, options).subscribe(reg => {
      if (reg.status === 201) {
        const userAccount: IUser = reg.json();
        this.addUserToLocalStorage(userAccount);
        this.router.navigate(['/home']);
      } else if (reg.status === 204) {
        this.toastr.error('Invalid email or password.', 'ERROR');
      }
    });
  }

  insertUserInDB(model: IRegister) {
    const url = this.urlService.register;

    const myHeaders = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    const options = new RequestOptions({ headers: myHeaders });
    const body = JSON.stringify(model);

    this.http.post(url, body, options).subscribe(reg => {
      if (reg.status === 201) {
        const newUser: IUser = {
          FirstName: model.firstName,
          LastName: model.lastName,
          Email: model.email,
          PhoneNumber: model.phoneNumber,
          Address: model.address
        };
        this.addUserToLocalStorage(newUser);
        this.router.navigate(['/home']);
      } else if (reg.status === 204) {
        this.toastr.error('User with the same email already exists.', 'ERROR');
      }
    });
  }

  addUserToLocalStorage(user: IUser) {
    window.localStorage.setItem('user', JSON.stringify({
      firstName: user.FirstName,
      lastName: user.LastName,
      email: user.Email,
      phoneNumber: user.PhoneNumber,
      address: user.Address
    }));
    this.getUserFromLocalStorage();
  }

  removeUserFromLocalStorage() {
    window.localStorage.removeItem('user');
    this.isAuthenticated = false;
  }

  getUserFromLocalStorage() {
    const data = window.localStorage.getItem('user');
    if (data != null) {
      this.isAuthenticated = true;

      const user = JSON.parse(data);
      this.authenticatedUser = {
        FirstName: user.firstName,
        LastName: user.lastName,
        Email: user.email,
        PhoneNumber: user.phoneNumber,
        Address: user.address
      };
    } else {
      this.isAuthenticated = false;
    }
  }

  isUserAuthenticated() {
    const data = window.localStorage.getItem('user');
    if (data != null) {
      this.isAuthenticated = true;

      const user = JSON.parse(data);
      this.authenticatedUser = {
        FirstName: user.firstName,
        LastName: user.lastName,
        Email: user.email,
        PhoneNumber: user.phoneNumber,
        Address: user.address
      };
    } else {
      this.isAuthenticated = false;
    }
  }

  getUserOrders(): Observable<IOrderInfo[]> {
    const url = this.urlService.userOrdersUrl + this.authenticatedUser.Email;
    return this.restService.get(url);
  }

}
