import { TestBed, inject } from '@angular/core/testing';

import { ProductsCatalogService } from './products-catalog.service';

describe('ProductsCatalogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsCatalogService]
    });
  });

  it('should be created', inject([ProductsCatalogService], (service: ProductsCatalogService) => {
    expect(service).toBeTruthy();
  }));
});
