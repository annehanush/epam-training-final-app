import { Injectable } from '@angular/core';
import { UrlService } from './url.service';

import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router/src';
import { ICategory } from './models/ICategory';
import { Observable } from 'rxjs/Observable';
import { RestService } from './rest.service';

@Injectable()
export class CategoryService {

  selectedCategory: ICategory;

  constructor(private urlService: UrlService,
              private restService: RestService,
              public http: Http) { this.http = http; }

  getCategories(): Observable<ICategory[]> {
    const url = this.urlService.categoryUrl;
    return this.restService.get(url);
  }

  selectCategory(category: ICategory): void {
    this.selectedCategory = category;
  }
}
