import { Component, OnInit } from '@angular/core';
import { AccountService } from '../core/account.service';

import { Router } from '@angular/router';
import { CartService } from '../core/cart.service';
import { IOrder } from '../core/models/IOrder';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {

  order: IOrder;
  totalPrice = 0;

  isUserAuthenticated: boolean;

  constructor(private accountService: AccountService,
              private cartService: CartService,
              private router: Router) { this.isUserAuthenticated = this.accountService.isAuthenticated; }

  ngOnInit() {
    this.loadOrderWithCartItems();
  }

  logOut() {
    this.accountService.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
  }

  loadOrderWithCartItems() {
    this.cartService.getCartItems().subscribe(
      data => {
        this.order = data;
        data.OrderItems.forEach(element => {
          this.totalPrice += element.ProductPrice;
        });
      },
      error => { console.error(error); });
  }

  removeItem(id: string) {
    const item = this.order.OrderItems.find(i => i.Id === id);
    const index = this.order.OrderItems.indexOf(item);
    if (index !== -1) {
        this.order.OrderItems.splice(index, 1);
        this.totalPrice -= item.ProductPrice;
    }

    this.cartService.removeCartItem(id);
  }

  submitCart(id: string) {
    this.cartService.submitCart(id);
  }

}
