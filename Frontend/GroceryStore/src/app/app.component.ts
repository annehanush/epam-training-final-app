import { Component } from '@angular/core';
import { AccountService } from './core/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'app';

  constructor(private accountService: AccountService) {
    this.accountService.isUserAuthenticated();
  }
}
