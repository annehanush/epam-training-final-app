import { Component, OnInit, Renderer, ElementRef } from '@angular/core';
import { ProductsCatalogService } from '../core/products-catalog.service';
import { IProduct } from '../core/models/IProduct';
import { ISubcategory } from '../core/models/ISubcategory';
import { Router } from '@angular/router';

import { GridOptions } from 'ag-grid';
import { GridComponent } from '../grid/grid.component';
import { GridAddToCartComponent } from '../grid-add-to-cart/grid-add-to-cart.component';
import { CategoryService } from '../core/category.service';
import { ICategory } from '../core/models/ICategory';
import { AccountService } from '../core/account.service';

@Component({
  selector: 'app-products-catalog',
  templateUrl: './products-catalog.component.html',
  styleUrls: ['./products-catalog.component.sass']
})
export class ProductsCatalogComponent implements OnInit {

  isUserAuthenticated: boolean;
  category: ICategory;

  gridOptions: GridOptions;
  columnDefs: any[]
  rowData: any[] = [];

  allProductsOfCategory: IProduct[];
  products: IProduct[];

  subcategories: ISubcategory[] = [];

  constructor(private productsCatalogService: ProductsCatalogService,
              private categoryService: CategoryService,
              private renderer: Renderer,
              private elRef: ElementRef,
              private accountService: AccountService,
              private router: Router) {
    this.isUserAuthenticated = this.accountService.isAuthenticated;
    this.category = categoryService.selectedCategory;

    this.gridOptions = <GridOptions>{};
    this.columnDefs = [
      { headerName: 'Image', field: 'image', cellRendererFramework: GridComponent },
      { headerName: 'Name', field: 'name' },
      { headerName: 'Price', field: 'price' },
      { headerName: 'Add to cart', field: 'addToCart', cellRendererFramework: GridAddToCartComponent },
    ];
    this.gridOptions.rowHeight = 150;
    this.gridOptions.rowStyle = { 'border-bottom': '1px solid #888', background: '#fff', 'line-height': '150px', 'text-align': 'center' };
    this.gridOptions.headerHeight = 35;
  }

  ngOnInit() {
    this.loadProductsCatalog();
  }

  // all products of selected category
  loadProductsCatalog() {
    this.productsCatalogService.getProductsCatalog().subscribe(
      data => {
        this.allProductsOfCategory = data;
        this.products = this.allProductsOfCategory;

        this.loadSubcategoriesOfCategory();

        this.generateGrid();
      },
      error => { console.error(error); });
  }

  loadSubcategoriesOfCategory() {
    this.allProductsOfCategory.forEach(product => {
      if (!this.subcategories.find(p => p.Id === product.Subcategory.Id)) {
        this.subcategories.push(product.Subcategory);
      }
    });
  }

  selectAll(event: any) {
    this.products = this.allProductsOfCategory;
    let element = this.elRef.nativeElement.querySelector('.selected');
    this.renderer.setElementClass(element, 'selected', false);
    this.renderer.setElementClass(event.target, 'selected', true);

    this.generateGrid();
  }

  selectByCategory(event: any, categoryId: string) {
    this.products = this.allProductsOfCategory.filter(subCat => subCat.Subcategory.Id === categoryId);

    let element = this.elRef.nativeElement.querySelector('.selected');
    this.renderer.setElementClass(element, 'selected', false);

    this.renderer.setElementClass(event.target, 'selected', true);

    this.generateGrid();
  }

  generateGrid() {
    let rows: any[] = [];
    this.products.forEach(element => {
      rows.push({ image: element.ImagePath, name: element.Name, price: element.Price, addToCart: element.Id });
    });
    this.rowData = rows;
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }

  logOut() {
    this.accountService.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
  }

}
