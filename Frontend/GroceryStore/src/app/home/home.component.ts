import { Component, OnInit } from '@angular/core';
import { ICategory } from '../core/models/ICategory';
import { CategoryService } from '../core/category.service';
import { Router } from '@angular/router';
import { AccountService } from '../core/account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  isUserAuthenticated: boolean;

  categories: ICategory[];
  selectedCategory: ICategory;

  constructor(private router: Router,
              private categoryService: CategoryService,
              private accountService: AccountService) { this.isUserAuthenticated = this.accountService.isAuthenticated; }

  ngOnInit() {
    this.loadCategories();
  }

  loadCategories() {
     this.categoryService.getCategories().subscribe(
      data => {
        this.categories = data;
      },
      error => { console.error(error); });
  }

  selectCategory(category: ICategory) {
    this.categoryService.selectCategory(category);
    this.router.navigate(['/products-сatalog']);
  }

  logOut() {
    this.accountService.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
  }

}
