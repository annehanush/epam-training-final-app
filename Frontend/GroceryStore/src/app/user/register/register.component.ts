import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { IRegister } from '../../core/models/IRegister';
import { AccountService } from '../../core/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      address: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit({ value, valid }: { value: IRegister, valid: Boolean }): void {
    this.accountService.isUserAuthenticated();

    if (this.accountService.isAuthenticated) {
      alert('You are already registered in system. To create a new profile exit the current one.');
    } else {
      this.accountService.insertUserInDB(value);
    }
  }

}
