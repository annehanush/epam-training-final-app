import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../core/account.service';

import { Router } from '@angular/router';
import { IUser } from '../../core/models/IUser';
import { IOrderInfo } from '../../core/models/IOrderInfo';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  isUserAuthenticated: boolean;
  user: IUser;
  orders: IOrderInfo[];

  constructor(private accountService: AccountService,
              private router: Router) {
      this.isUserAuthenticated = this.accountService.isAuthenticated;
      this.user = this.accountService.authenticatedUser;
    }

  ngOnInit() {
    this.loadUserOrders();
  }

  loadUserOrders() {
    this.accountService.getUserOrders().subscribe(
      data => {
        this.orders = data;
      },
      error => { console.error(error); });
  }

  logOut() {
    this.accountService.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
  }

}
