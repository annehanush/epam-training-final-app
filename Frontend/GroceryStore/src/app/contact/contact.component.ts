import { Component, OnInit } from '@angular/core';
import { AccountService } from '../core/account.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  isUserAuthenticated: boolean;

  constructor(private accountService: AccountService,
              private router: Router) { this.isUserAuthenticated = this.accountService.isAuthenticated; }

  ngOnInit() {
  }

  logOut() {
    this.accountService.removeUserFromLocalStorage();
    this.router.navigate(['/login']);
  }

}
