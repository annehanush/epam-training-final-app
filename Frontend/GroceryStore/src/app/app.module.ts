import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { CoreModule } from './core/core.module';
import { ContactComponent } from './contact/contact.component';
import { ProductsCatalogComponent } from './products-catalog/products-catalog.component';

import { AgGridModule } from 'ag-grid-angular/main';
import { GridComponent } from './grid/grid.component';
import { GridAddToCartComponent } from './grid-add-to-cart/grid-add-to-cart.component';
import { UserModule } from './user/user.module';
import { CartComponent } from './cart/cart.component';

import { ToastrModule } from 'ngx-toastr';
import { FooterComponent } from './footer/footer.component';
import { FaqComponent } from './faq/faq.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    ProductsCatalogComponent,
    GridComponent,
    GridAddToCartComponent,
    CartComponent,
    FooterComponent,
    FaqComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    AgGridModule.withComponents(
      [GridComponent, GridAddToCartComponent]
    ),
    UserModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
