import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAddToCartComponent } from './grid-add-to-cart.component';

describe('GridAddToCartComponent', () => {
  let component: GridAddToCartComponent;
  let fixture: ComponentFixture<GridAddToCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridAddToCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAddToCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
