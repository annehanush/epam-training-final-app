import { Component, OnInit } from '@angular/core';

import { AccountService } from '../core/account.service';
import { CartService } from '../core/cart.service';
import { IAddToCart } from '../core/models/IAddToCart';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-grid-add-to-cart',
  templateUrl: './grid-add-to-cart.component.html',
  styleUrls: ['./grid-add-to-cart.component.sass']
})
export class GridAddToCartComponent {

  private params: any;

  swal: any;

  constructor(private accountService: AccountService,
              private cartService: CartService,
              private toastr: ToastrService) {
  }

  agInit(params: any): void {
    this.params = params;
  }

  addToCart(id: string) {
    if (this.accountService.isAuthenticated) {
      const addToCart: IAddToCart = { ProductId: id, UserEmail: this.accountService.authenticatedUser.Email };
      this.cartService.addCartItem(addToCart);
    } else {
      this.toastr.error('To add items to cart you must be authenticated!', 'ERROR');
    }
  }


}
